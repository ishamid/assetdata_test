import axios from "axios";

export const URLS = "https://petronasnew2.be.assetd.xyz/api/v1";
export const TOKEN = sessionStorage.getItem("token");

export default {
  actions: {
    auth_base(mail, pass) {
      let data = {
        email: mail,
        password: pass,
        expire: 3600,
        type: "automatic",
      };

      return new Promise((resolve, reject) => {
        try {
          axios.post(URLS + `/a/hash-login`, data).then((response) => {
            if (response.status === 200) {
              sessionStorage.setItem("token", response.data.token);
              sessionStorage.setItem("exp", response.data.expire);
              window.location.href = "/landing";
              resolve(response);
            } else {
              resolve("login invalid");
              console.log("invalid");
            }
          });
        } catch (error) {
          reject(error);
        }
      });
    },

    get_profile() {
      return new Promise((resolve, reject) => {
        try {
          axios
            .get(URLS + `/a/me`, {
              headers: { Authorization: `Bearer ${TOKEN}` },
            })
            .then((response) => {
              if (response.status === 200) {
                resolve(response.data);
              } else {
                resolve("auth detail error");
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    },

    get_company() {
      let search = {
        orderCol: "name",
        orderDir: "asc",
        limit: 100,
        keyword: "",
      };
      return new Promise((resolve, reject) => {
        try {
          axios
            .get(URLS + `/setting/company`, {
              params: search,
              headers: { Authorization: `Bearer ${TOKEN}` },
            })
            .then((response) => {
              if (response.status === 200) {
                resolve(response.data);
              } else {
                resolve("datatable is error");
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    },

    submit_company(data) {
      return new Promise((resolve, reject) => {
        try {
          axios
            .post(URLS + `/setting/company/`, data, {
              headers: { Authorization: `Bearer ${TOKEN}` },
            })
            .then((response) => {
              if (response.status === 200) {
                window.location.href = "/company";
                resolve(response.data);
              } else {
                resolve("datatable is error");
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    },

    edit_company(data, uid) {
      return new Promise((resolve, reject) => {
        try {
          axios
            .put(URLS + `/setting/company/` + uid, data, {
              headers: { Authorization: `Bearer ${TOKEN}` },
            })
            .then((response) => {
              if (response.status === 200) {
                window.location.href = "/company";
                resolve(response.data);
              } else {
                resolve("datatable is error");
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    },

    delete_company(uid) {
      let req = {
        confirmDelete: true,
      };
      return new Promise((resolve, reject) => {
        try {
          axios
            .delete(URLS + `/setting/company/` + uid, {
              params: req,
              headers: { Authorization: `Bearer ${TOKEN}` },
            })
            .then((response) => {
              if (response.status === 200) {
                resolve(response.data);
              } else {
                resolve("datatable is error");
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    },
  },
};
