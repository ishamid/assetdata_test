import { Badge, Button, Col, Modal, Row, Table } from "react-bootstrap";
import StyledSection from "../../component/Section";
import StyledCard from "../../component/Card";
import StyledTypograph from "../../component/Typograph";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrash,
  faPenToSquare,
  faEye,
  faTriangleExclamation,
} from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import global from "./../../services/global";
import React, { PureComponent } from "react";
import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from "recharts";

function Monitor() {
  const data = [
    { name: "Test Company V1", value: 3 },
    { name: "New Company", value: 18 },
    { name: "Test Company V2", value: 7 },
    { name: "New Companys", value: 14 },
    { name: "Petronas Barhad", value: 55 },
  ];
  const dept = [
    { name: "Dept 3", value: 20 },
    { name: "per Departmen", value: 20 },
    { name: "Group Digital", value: 40 },
  ];
  const COLORS = ["#0C78F9", "#1CE283", "#FDA118", "#FB2A4E", "#6343C5"];

  return (
    <div className="List">
      <StyledSection>
        <StyledCard>
          <Row>
            <Col>
              <StyledTypograph size="18px" weight="600">
                Dashboard
              </StyledTypograph>
            </Col>
          </Row>
        </StyledCard>

        <StyledCard>
          <Row>
            <Col>
              <PieChart width={800} height={400}>
                <Pie
                  data={data}
                  cx={100}
                  cy={100}
                  innerRadius={50}
                  outerRadius={80}
                  fill="#8884d8"
                  paddingAngle={5}
                  dataKey="value"
                >
                  {data.map((entry, index) => (
                    <Cell
                      key={`cell-${index}`}
                      fill={COLORS[index % COLORS.length]}
                    />
                  ))}
                </Pie>

                <Pie
                  data={dept}
                  cx={300}
                  cy={100}
                  innerRadius={50}
                  outerRadius={80}
                  fill="#8884d8"
                  paddingAngle={5}
                  dataKey="value"
                >
                  {data.map((entry, index) => (
                    <Cell
                      key={`cell-${index}`}
                      fill={COLORS[index % COLORS.length]}
                    />
                  ))}
                </Pie>
              </PieChart>
            </Col>
          </Row>
        </StyledCard>
      </StyledSection>
    </div>
  );
}

export default Monitor;
