import { Button, Form } from "react-bootstrap";
import StyledCard from "../component/Card";
import StyledSection from "../component/Section";
import { useState } from "react";
import global from "./../services/global";

function Login() {
  useState(() => {}, []);

  const handleSubmit = (e) => {
    console.log("eeee");
    e.preventDefault();
    let email = e.target.mail.value;
    let password = e.target.password.value;
    global.actions.auth_base(email, password);
  };

  return (
    <div className="Login">
      <StyledSection align="center">
        <StyledCard>
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="mail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                name="email"
                placeholder="Enter your email address"
              />
            </Form.Group>

            <Form.Group className="mb-5" controlId="pass">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                name="password"
                placeholder="Type your password"
              />
            </Form.Group>

            <Form.Group className="mb-3 d-grid gap-2">
              <Button variant="primary" type="submit">
                SIGN IN
              </Button>
            </Form.Group>

            <Form.Group className="mb-3 d-grid gap-2">
              <Button variant="light" type="button" href="/">
                FORGOT PASSWORD
              </Button>
            </Form.Group>
          </Form>
        </StyledCard>
      </StyledSection>
    </div>
  );
}

export default Login;
