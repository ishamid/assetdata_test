import { Badge, Button, Col, Form, Row } from "react-bootstrap";
import StyledSection from "../../component/Section";
import StyledTypograph from "../../component/Typograph";
import Image from "react-bootstrap/Image";
import StyledCard from "../../component/Card";
import { useEffect, useState } from "react";
import global from "./../../services/global";

function Profile() {
  const [profile, setProfile] = useState([]);

  useEffect(() => {
    global.actions.get_profile().then((res) => {
      // console.log(res.data);
      console.log("bilaaa", res.data);
      setProfile(res.data);
    });
  }, []);

  return (
    <div className="Profile">
      <StyledSection>
        <StyledCard>
          <Row className="mb-4">
            <Col md={2}>
              <Image
                src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
                thumbnail
              />
            </Col>

            <Col md={10}>
              <StyledTypograph size="16px" weight="600">
                Account Admin
              </StyledTypograph>
              <StyledTypograph>{profile?.email}</StyledTypograph>
              <Badge bg="primary mx-1">{profile?.role_name}</Badge>
            </Col>
          </Row>

          <Row>
            <Col md={6}>
              <Form.Group className="mb-3" controlId="mail">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                  name="first_name"
                  placeholder="Enter your first name"
                  defaultValue={profile?.first_name}
                  required
                />
              </Form.Group>
            </Col>
            <Col md={6}>
              <Form.Group className="mb-3" controlId="mail">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                  name="last_name"
                  placeholder="Enter your last name"
                  defaultValue={profile?.last_name}
                  required
                />
              </Form.Group>
            </Col>
            <Col md={6}>
              <Form.Group className="mb-3" controlId="company">
                <Form.Label>Company</Form.Label>
                <Form.Select
                  name="company"
                  placeholder="Select your company"
                  required
                >
                  <option>{profile.company ? profile.company.name : ""}</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col md={6}>
              <Form.Group className="mb-3" controlId="department">
                <Form.Label>Department</Form.Label>
                <Form.Select
                  name="department"
                  placeholder="Select your department"
                >
                  <option>
                    {profile.company_department
                      ? profile.company_department.name
                      : ""}
                  </option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col md={6}>
              <Form.Group className="mb-3" controlId="jobs">
                <Form.Label>Job Title</Form.Label>
                <Form.Control
                  name="job_title"
                  placeholder="Enter job title"
                  defaultValue={profile?.job_title}
                  required
                />
              </Form.Group>
            </Col>
            <Col md={6}>
              <Form.Group className="mb-3" controlId="employee_id">
                <Form.Label>Employee ID</Form.Label>
                <Form.Control
                  name="employee_id"
                  placeholder="Add employee ID"
                  defaultValue={profile?.employee_number}
                  required
                />
              </Form.Group>
            </Col>
            <Col md={6}>
              <Form.Group className="mb-3" controlId="employee_id">
                <Form.Label>Phone Number</Form.Label>
                <Row>
                  <Col md={6} className="mb-3">
                    <Form.Select name="region" placeholder="Select region">
                      <option>
                        {profile.preference
                          ? profile.preference.timezone.split(":")[0]
                          : ""}{" "}
                        (+
                        {profile.phone_number
                          ? profile.phone_number.split(" ")[0]
                          : ""}
                        )
                      </option>
                    </Form.Select>
                  </Col>
                  <Col md={6} className="mb-3">
                    <Form.Control
                      name="phone"
                      type="tel"
                      placeholder="Use phone number"
                      defaultValue={
                        profile.phone_number
                          ? profile.phone_number.split(" ")[1]
                          : ""
                      }
                    />
                  </Col>
                </Row>
              </Form.Group>
            </Col>
          </Row>
        </StyledCard>

        <Row className="justify-content-end">
          <Col md={2} className="my-3 d-grid gap-2">
            <Button variant="primary" href="/landing">
              Save
            </Button>
          </Col>
        </Row>
      </StyledSection>
    </div>
  );
}

export default Profile;
