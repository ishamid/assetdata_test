import { Badge, Button, Col, Form, Row } from "react-bootstrap";
import StyledSection from "../../component/Section";
import StyledTypograph from "../../component/Typograph";
import Image from "react-bootstrap/Image";
import StyledCard from "../../component/Card";
import { useEffect, useState } from "react";
import global from "./../../services/global";

function FormCompany() {
  const [uid, setUid] = useState("");

  useEffect(() => {
    setUid(sessionStorage.getItem("uid"));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    let data = {
      address_1: e.target.address_1.value,
      address_2: e.target.address_2.value,
      city: e.target.city.value,
      country_code: "AL",
      financial_year_begin: "02-01",
      name: e.target.name.value,
      postcode: e.target.postcode.value,
      registration_number: e.target.registration_number.value,
      state: e.target.state.value,
      status: 1,
      street: e.target.street.value,
      tax_number: e.target.tax_number.value,
    };

    console.log(data);

    if (uid) {
      global.actions.edit_company(data, uid).then((res) => {
        sessionStorage.removeItem("uid");
      });
    } else {
      global.actions.submit_company(data);
    }
  };

  return (
    <div className="FormCompany">
      <Form onSubmit={handleSubmit}>
        <StyledSection>
          <StyledCard>
            <Row className="border-bottom">
              <Col md={10}>
                <StyledTypograph
                  padding="5px 0px 20px"
                  size="18px"
                  weight="600"
                >
                  Add Company
                </StyledTypograph>
              </Col>
            </Row>

            <Row className="mb-3">
              <Col md={12} className="border-bottom mb-3">
                <StyledTypograph padding="5px 0px" size="16px" weight="600">
                  Company Info
                </StyledTypograph>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="name">
                  <Form.Label>Company Name</Form.Label>
                  <Form.Control
                    name="name"
                    placeholder="Enter company name"
                    required
                  />
                </Form.Group>
              </Col>
              <Col md={{ span: 3, offset: 3 }}>
                <Image
                  src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
                  thumbnail
                />
              </Col>
            </Row>

            <Row className="mb-3">
              <Col md={12} className="border-bottom mb-3">
                <StyledTypograph padding="5px 0px" size="16px" weight="600">
                  Address
                </StyledTypograph>
              </Col>
              <Col md={12}>
                <Form.Group className="mb-3" controlId="address_1">
                  <Form.Label>Address Field 1</Form.Label>
                  <Form.Control
                    name="address_1"
                    placeholder="Enter address field 1"
                    required
                  />
                </Form.Group>
              </Col>
              <Col md={12}>
                <Form.Group className="mb-3" controlId="address_2">
                  <Form.Label>Address Field 2</Form.Label>
                  <Form.Control
                    name="address_2"
                    placeholder="Enter address field 2"
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="country_code">
                  <Form.Label>Country</Form.Label>
                  <Form.Select
                    name="country_code"
                    placeholder="Select your country"
                  >
                    <option> select</option>
                  </Form.Select>
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="state">
                  <Form.Label>State</Form.Label>
                  <Form.Control
                    name="state"
                    placeholder="Enter State"
                    disabled
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="city">
                  <Form.Label>City</Form.Label>
                  <Form.Control name="city" placeholder="Enter city" disabled />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="street">
                  <Form.Label>Street/Building</Form.Label>
                  <Form.Control
                    name="street"
                    placeholder="Enter street/building"
                    disabled
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="postcode">
                  <Form.Label>Postal Code/ZIP</Form.Label>
                  <Form.Control
                    name="postcode"
                    placeholder="Enter Postal Code/ZIP"
                  />
                </Form.Group>
              </Col>
            </Row>

            <Row className="mb-3">
              <Col md={12} className="border-bottom mb-3">
                <StyledTypograph padding="5px 0px" size="16px" weight="600">
                  Other Info
                </StyledTypograph>
              </Col>

              <Col md={6}>
                <Form.Group className="mb-3" controlId="registration_number">
                  <Form.Label>Registration ID</Form.Label>
                  <Form.Control
                    name="registration_number"
                    placeholder="Enter Registration ID"
                  />
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className="mb-3" controlId="tax_number">
                  <Form.Label>Tax ID</Form.Label>
                  <Form.Control name="tax_number" placeholder="Enter Tax ID" />
                </Form.Group>
              </Col>
            </Row>
          </StyledCard>

          <Row className="justify-content-end">
            <Col md={2} className="my-3 d-grid gap-2">
              <Button type="button" variant="secondary">
                Reset
              </Button>
            </Col>
            <Col md={2} className="my-3 d-grid gap-2">
              <Button type="submit" variant="primary">
                Save
              </Button>
            </Col>
          </Row>
        </StyledSection>
      </Form>
    </div>
  );
}

export default FormCompany;
