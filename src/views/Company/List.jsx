import { Badge, Button, Col, Modal, Row, Table } from "react-bootstrap";
import StyledSection from "../../component/Section";
import StyledCard from "../../component/Card";
import StyledTypograph from "../../component/Typograph";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrash,
  faPenToSquare,
  faEye,
  faTriangleExclamation,
} from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import global from "./../../services/global";

function List() {
  const [company, setCompany] = useState([]);
  const [show, setShow] = useState(false);
  const [uid, setUid] = useState("");

  useEffect(() => {
    onLoad();
  }, []);

  const onLoad = () => {
    global.actions.get_company().then((res) => {
      console.log(res.data);
      setCompany(res.data);
    });
  };

  const onEdit = (event) => {
    console.log(event);
    sessionStorage.setItem("uid", event);
    window.location.href = "/form-company";
  };

  const handleClose = () => {
    setUid("");
    setShow(false);
  };
  const handleShow = (uid) => {
    setShow(true);
    setUid(uid);
  };

  const onDelete = (idx) => {
    global.actions.delete_company(idx).then((res) => {
      console.log(res.data);
      onLoad();
    });
  };

  return (
    <div className="List">
      <StyledSection>
        <StyledCard>
          <Row className="justify-content-center mb-3">
            <Col md={6}>
              <StyledTypograph size="18px" weight="600">
                Companies
              </StyledTypograph>
            </Col>
            <Col md={{ span: 3, offset: 2 }}>
              <Button
                variant="primary"
                className="d-grid gap-2"
                size="sm"
                href="/form-company"
              >
                + Add New Company
              </Button>
            </Col>
            <Col md={1}>
              <Button variant="outline-dark" size="sm" className="d-grid gap-2">
                Filter
              </Button>
            </Col>
          </Row>

          <div className="table-reponsive">
            <Table hover responsive="sm">
              <thead>
                <tr className="bg-primary-secondary">
                  <th className="">View</th>
                  <th className="w-25">Company Name</th>
                  <th className="w-50">Address</th>
                  <th className="w-25">Action</th>
                </tr>
              </thead>
              <tbody>
                {company.map((field, i) => {
                  return (
                    <tr key={i}>
                      <td className="">
                        <Button
                          variant="outline-primary"
                          size="sm"
                          className="m-1"
                        >
                          <FontAwesomeIcon icon={faEye} />
                        </Button>
                      </td>
                      <td className="w-25" valign="middle">
                        {field.name}
                      </td>
                      <td className="w-50" valign="middle">
                        {field.address}
                      </td>
                      <td className="w-25">
                        <Button
                          variant="outline-primary"
                          size="sm"
                          className="m-1"
                          onClick={() => onEdit(field.guid)}
                        >
                          <FontAwesomeIcon icon={faPenToSquare} />
                        </Button>
                        <Button
                          variant="outline-danger"
                          size="sm"
                          className="m-1"
                          onClick={() => handleShow(field.guid)}
                        >
                          <FontAwesomeIcon icon={faTrash} />
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        </StyledCard>

        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Delete Company</Modal.Title>
          </Modal.Header>
          <Modal.Body className="d-flex my-0">
            <div className="action_delete" bg="danger">
              <FontAwesomeIcon icon={faTriangleExclamation} />
            </div>
            <StyledTypograph size="14px" padding="15px">
              Are you sure to delete ?
            </StyledTypograph>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={() => onDelete(uid)}>
              Delete
            </Button>
          </Modal.Footer>
        </Modal>
      </StyledSection>
    </div>
  );
}

export default List;
