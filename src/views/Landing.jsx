import { Row, Col, Image } from "react-bootstrap";
import StyledCard from "../component/Card";
import StyledSection from "../component/Section";
import StyledTypograph from "../component/Typograph";

function Landing() {
  const data = [
    {
      url: "/profile",
      name: "Profile",
      img: "",
    },
    {
      url: "/dashboard",
      name: "Dashboard",
      img: "",
    },
    {
      url: "/company",
      name: "Company",
      img: "",
    },
  ];

  const handle = (e) => {
    window.location.href = e;
  };

  return (
    <div className="Login">
      <StyledSection align="center">
        <Row className="justify-content-center">
          {data.map((res, i) => {
            return (
              <Col md={2} key={i}>
                <StyledCard onClick={() => handle(res.url)} padding="5px">
                  <Image src={res.img} thumbnail fluid />
                  <StyledTypograph align="center">{res.name}</StyledTypograph>
                </StyledCard>
              </Col>
            );
          })}
        </Row>
      </StyledSection>
    </div>
  );
}

export default Landing;
