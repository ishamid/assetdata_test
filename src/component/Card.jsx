import PropTypes from "prop-types";
import { Card } from "react-bootstrap";

export default function StyledCard({ bgcolor, children, padding, onClick }) {
  const styled = {
    backgroundColor: bgcolor ? bgcolor : "#fff",
    borderColor: "transparent",
    borderRadius: "10px",
    padding: padding ? padding : "10px",
  };

  return (
    <Card style={styled} onClick={onClick}>
      <Card.Body>{children}</Card.Body>
    </Card>
  );
}

StyledCard.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
  bgcolor: PropTypes.string,
  padding: PropTypes.string,
  part: PropTypes.string,
};
