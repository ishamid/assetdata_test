import PropTypes from "prop-types";
import { Container } from "react-bootstrap";

export default function StyledSection({
  bgcolor,
  children,
  padding,
  part,
  align,
}) {
  const styled = {
    backgroundColor: bgcolor ? bgcolor : "#EEF0F7",
    border: "none",
    minHeight: "100vh",
    padding: padding ? padding : "50px 0",
    position: "relative",
    justifyContent: align ? align : "left",
    alignContent: align ? align : "left",
    alignItems: align ? align : "left",
    display: "flex",
  };

  return (
    <div style={styled} id={part}>
      <Container>{children}</Container>
    </div>
  );
}

StyledSection.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
  bgcolor: PropTypes.string,
  padding: PropTypes.string,
  part: PropTypes.string,
  align: PropTypes.string,
};
