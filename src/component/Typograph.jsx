import PropTypes from "prop-types";
export default function StyledTypograph({
  fill,
  children,
  padding,
  color,
  size,
  weight,
  align,
}) {
  const styled = {
    fill: fill ? fill : "transparent",
    color: color ? color : "#222  ",
    padding: padding ? padding : "5px",
    fontSize: size ? size : "13px",
    fontWeight: weight ? weight : "400",
    textAlign: align ? align : "left",
    marginBottom: "2px",
  };

  return <p style={styled}>{children}</p>;
}

StyledTypograph.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string,
  ]),
  fill: PropTypes.string,
  padding: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.string,
  weight: PropTypes.string,
  align: PropTypes.string,
};
