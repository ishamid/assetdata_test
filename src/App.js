import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./views/Login";
import Landing from "./views/Landing";
import Profile from "./views/Profile/Profile";
import CompanyList from "./views/Company/List";
import CompanyForm from "./views/Company/Form";
import Dashboard from "./views/Dashboard/Monitor";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Login />} />
          <Route exact path="/landing" element={<Landing />} />
          <Route exact path="/profile" element={<Profile />} />
          <Route exact path="/company" element={<CompanyList />} />
          <Route exact path="/form-company" element={<CompanyForm />} />
          <Route exact path="/dashboard" element={<Dashboard />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
